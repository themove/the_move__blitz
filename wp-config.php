<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'blitz' );

/** Имя пользователя MySQL */
define( 'DB_USER', 'root' );

/** Пароль к базе данных MySQL */
define( 'DB_PASSWORD', '' );

/** Имя сервера MySQL */
define( 'DB_HOST', 'localhost' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '/BW^I .bdBZC:c>l9`iGO6gyPTFIxUZiCS4f4/<%4L?r8+S }6+Ex[@+#]]T`|P}' );
define( 'SECURE_AUTH_KEY',  'FSwR@W;4yLO6{ m+Er%(G`s{3xL*n]NBW+7{t#e n9~(!?+TI}#+*w$03Qswa/l]' );
define( 'LOGGED_IN_KEY',    'j24D/?O!b?o_A9XR,z4anr$`a#8}~5LI.`9g{TNqH47;=ftpK*y1W:[BxOSgv!_l' );
define( 'NONCE_KEY',        '8)_o+L#K1xsjcG2,2y0Z-gYr|+382&4*;Pu@.S:Hprxl<nT.~!juWjqPqIRH2rE9' );
define( 'AUTH_SALT',        '*?!bZ}<<KpEj:io>-/^:>IJ_ItQupz)HTX-5$m-_z#.XM$gBWX[mDPs8|SE7MC8N' );
define( 'SECURE_AUTH_SALT', ' aMfIe]FfOfAi1HL$4r[gp%OG$9^/ g/ompd Cx>_-B^^Q^[u,UaI#U3.s:(|5>k' );
define( 'LOGGED_IN_SALT',   '^r$u1>g:7W>5<nF-|2C3.oFr)lnlz4/J8P1r0^hgy{@IE:Bq){WBo[%r2C+$(Eph' );
define( 'NONCE_SALT',       ':Y*!4D2:2a{1$O#=kdy?@81@)5lNPLo[Swxx6Bk7*}Vc!-D}1 j,bg+k8nO*E<|{' );

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once( ABSPATH . 'wp-settings.php' );
